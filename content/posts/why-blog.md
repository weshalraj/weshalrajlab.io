---
title: "Why Start A Blog?"
date: 2020-12-15T11:30:03+00:00
# weight: 1
# aliases: ["/first"]
tags: ["first", "why blog"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "A monologue about why I am starting a blog & why everyone should consider too."
canonicalURL: "https://vishalraj.xyz/posts/why-blog/"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/weshallraj/weshallraj.gitlab.io/-/blob/master/content/"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
>TLDR: Its just a place to slap text we don't have a better place for. There is no particular theme to these writings. 

I've wanted to start writing for the longest time. But something inside me has kept stalling it. Below is an introspection exercise that aims to infer
- What's been stopping me. 
- What's in there for me (or for anyone for that matter), & 
- What I intent to achieve via this medium of expression. 

## __The struggle__
___
As of today, there are plenty of technical choices when it comes to starting a blog. One could start atleast five different blogs in the next 30 minutes, each one on a different platform/infrastructure. Not to mention, most of these options would be free of cost (some wouldn't even require details like credit card info, etc.) 

Instead, what I've mostly struggled with is something interesting to write about *consistently*. I have been writing code for sometime now and I happen to be working on a startup that does use some blog-worthy tech. But any attempt to pen down some of the interesting stuff results in a very long essay-shaped posts, which are extremely time-consuming to write and frankly not so interesting from a reader's standpoint as well.

> The struggle is real when it comes to finding a niche and consistently write about it.

## __The Possibilities__
___

- As someone who struggles with micro attention spans, consciously jotting down thoughts and learnings could potentially with my thinking process and articulations. 

- I genuinely want to develop a habit to write. Blogging could make that possible.

- This website also gives me a chance to tinker and explore new ways to organize my website. Integrate with a CMS, add a newsletter, test UI/UX design decisions, run SEO experiments, the possibilites are endless.

- By putting myself out there, blogging could possibly help me with my committment issues. Because sometimes, publishing forces oneself to a higher standard of care. The mere possibility of feedback or a connection feels worth it. 

- Frankly, I hate posting on social media as it consequents to a crippling dependency of instant gratification and the fear of irrelavancy. A blogging medium makes it possible to avoid having to deal with the arbitrary constraints (like a 280 char limit) without compromising my ease to express. Endless possible customisations is an added bonus. 

## __The Intent__
___

I intend to 
- write about things I want to tell people about. Maybe we had an interesting lunchtime conversation, I was stuck with a silly bug at my workplace for days, something unusual came up, I'll read about it more and write a post. Or maybe there's a decision I'm making: I'll write up how I'm thinking about it, where I am so far, and publish that. This could help me think about the problem differently. Anecdotally, I've observed that writing an email at work to describe a production incident or a feature requirments helps me gain fresh perspective to the problem at hand.

- try to notice when I get ideas for good blog worthy posts and write them down. Later, I'll probably sit down, look over the ideas, and see if there's anything I feel like writing about.

- I might use this platform as a way to jot down some of my admin/git workflows or general tidbits of information that i might need to lookup in he future (in cases like device migration), browsing a website to find the same might prove to be more efficient as compared to a notion page or a github gist (controversial).

---
I do not intend to 

- Track engagment. I want this to be an outlet of information hosted on on my online home. If a discussion on that information is required, it can be presented in a moderated forum and discussed if the participants deem the content *discussion worthy.*
- Write to be read. Rather write for myself. Rather write because I enjoy the process and the exposition that helps me process and crystallise a concept. Rather write because I'm interested and engaged in a certain topic. This argument generally leads to:
>Q: But then why blog? Why not just journal in a notebook or on your "notes" app and be done?

Ofcourse. It can definitely be done in a private journal and would I'm likely to experience many of the same effects. But know myself, I feel having my musings online gives other people a chance to learn from it as well. Its only lately that I have come to realize that small musings, bits of knowledge - when put out in the world - can lead to a multitude of possibilites. This blog will mostly will mostly be a conversation with myself. 


I have always felt immense pressure mostly because of the current zeigeist that your blog is your platform to market yourself. This adds an enormous amount scrutiny for doing quality and interesting posts as if you're attempting to publish a paper into a prestigious journal. I'm done with that. These days the main audience for my blog is myself. I like writing and I'm equally prone to write about the coffee I just had or about some clever coding thing I learnt or came up with.
