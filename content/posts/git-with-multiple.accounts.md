---
title: "Use different emails for personal and work git repos on the same machine"
date: 2020-12-21T02:34:03+00:00
# weight: 1
# aliases: ["/first"]
tags: ["git", "things I forget"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "How to setup different git environments with different accounts"
canonicalURL: "https://vishalraj.xyz/posts/git-with-multiple-accounts/"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/weshallraj/weshallraj.gitlab.io/-/blob/master/content/"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

> TLDR: Steps to reproduce when you want to use different emails for personal and work repositories on the same machine


Previously, I've encountered situations where I've had to dabble between personal and work profiles on the same machine. 
Therefore, I tend to separate my personal and work projects in different folders. But that's not enough. I need to set different git profiles for the personal and work folders for different commit authors. This is the part which I tend to forget. Below is the set of steps which help me achieve that.

This also comes handy as I use different emails for github and gitlab (yes, I'm a monster that way)

---
## Prerequisites


Say you create two directories ```/work``` and ```/personal``` in your home home ```/~``` directory.

and you have a ```~/.gitconfig``` file residing in your home (```~```) directory.

---

## Steps

- open the ```~/.gitconfig``` file and add the below config. Change ```YOUR_WORK_FOLDER_NAME``` and ```YOUR_PERSONAL_FOLDER_NAME``` as the names suggest. Since my folders are called work and personal, below will be the config for me 

```
[user]
	name = Captain Vyom
	email = captainvyom@example.com

[includeIf "gitdir:~/work/"]
    path = ~/work/.work.gitconfig

[includeIf "gitdir:~/personal/"]
    path = ~/personal/.personal.gitconfig
```
- as the above config suggests, your global git config (the one that resides in home (```~```)), will look for additional .gitconfigs *if* the path matches 
    - ```~/work/``` , or 
    - ```~/personal/```
- again, as the config suggests, the additional .gitconfigs, in my case, are named ```.work.gitconfig``` and ```.personal.gitconfig``` and the global gitconfig tells git where to look for these files. So you need to 
    - create ```.personal.gitconfig``` file inside ```~/personal/``` and add below config
    ```
    [user]
        name = personal Vyom
        email = personalemail@example.com
    ```
    - create a ```.work.gitconfig``` file inside ```~/work/``` and add below config
    ```
    [user]
        name = professional Vyom
        email = workemail@example.com
    ```

... and that should do it.

---
## (Bonus tip) How to Test

In your global gitconfig i.e. ```~/.gitconfig```, add the below git alias 

```
[alias]
  # Show verbose output about tags, branches or remotes
  whichone = "! git var -l | grep '^GIT_.*_IDENT'"
  tags = tag -l
  branches = branch -a
  remotes = remote -v
  # Pretty log output
  hist = log --graph --pretty=format:'%Cred%h%Creset %s%C(yellow)%d%Creset %Cgreen(%cr)%Creset [%an]' --abbrev-commit --date=relative
```
i call my alias ```whichone``` as a personal preference. You can call it anything. 

Then you can try using it in your terminal to determine which email and name will be used as git author when. In the case explained above, the output would look something like this.

```
# Inside work folder

~/work: git whichone
GIT_COMMITTER_IDENT=professional Vyom <workemail@example.com> 
GIT_AUTHOR_IDENT=professional Vyom <workemail@example.com> 

# Inside personal folder

~/personal: git whichone
GIT_COMMITTER_IDENT=personal Vyom <personalemail@example.com> 
GIT_AUTHOR_IDENT=personal Vyom <personalemail@example.com> 

# Any path other than /personal or /work

~/somepath: $ git whichone
GIT_COMMITTER_IDENT=Captain Vyom <captainvyom@example.com>
GIT_AUTHOR_IDENT=Captain Vyom <captainvyom@example.com>
```

---
This has worked well for me till date. But my choice of OS is Ubuntu/MacOS. If you face some problem when using ming64 on windows, do let me know.   